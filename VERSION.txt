2.0.7 Rename allocationId to allocationUnitId in SandboxInfoCreateDto.
2.0.6 Increase maximum header size limit.
2.0.5 Add sandbox allocation id to migration db.
2.0.4 Add an endpoint to delete sandbox references and answers by allocation ID.
22.12-rc.1 Changed sandboxId type to string.
2.0.3 Removed stacktrace from errors.
2.0.2 Modify URL of method to obtain answers for local sandbox.
2.0.1 Modify config of swagger documentation, DB size of answer column changed to 2048, storage of answers for local sandboxes.
2.0.0 Migrate project to Java 17.
1.0.9 Updated dependencies to the latest versions. Code cleanup. Configured multistage docker build to decrease the resulting image size.
1.0.8 Update log4j2 version to avoid CVE-2021-44228 vulnerability
1.0.7 Return SandboxInfoDTO in findAnswersForParticularSandbox endpoint.
1.0.6 New endpoint - get answer by sandbox id and answer variable name, fixed create and delete requests, integration of the Swagger.
1.0.5 Gitlab CI configuration and other usefull operations added.
